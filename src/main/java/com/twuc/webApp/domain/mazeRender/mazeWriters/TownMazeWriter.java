package com.twuc.webApp.domain.mazeRender.mazeWriters;

import org.springframework.stereotype.Component;

@Component
public class TownMazeWriter extends Fake3DMazeWriter {
    public TownMazeWriter(TownMazeComponentFactory factory) {
        super(factory);
    }

    @Override
    public String getName() {
        return "town";
    }
}
